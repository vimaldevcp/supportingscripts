from wand.image import Image
import sys

if len(sys.argv) > 1:
	parameter_value = sys.argv[1]
#parameter_value = 1561772333133422
	print("Received parameter from Perl script:", parameter_value)
else:
	print("No parameter received from Perl script.")

# Convert PDF to an image
with Image(filename='/opt/mobileapp/www/web/pdfFiles/receipt_'+ str(parameter_value) +'.pdf', resolution=300) as img:
    img.compression_quality = 99
    img.save(filename='/opt/mobileapp/www/web/pdfFiles/pdfImage/original_'+ str(parameter_value) +'.jpg')

# Resize the original image to a lower resolution
with Image(filename='original.jpg') as img_low_res:
    img_low_res.resize(220, 0)  # Width is set to 220, height is adjusted proportionally
    img_low_res.save(filename='lowRes.jpg')

# Resize the original image to a thumbnail
with Image(filename='original.jpg') as img_thumb:
    img_thumb.resize(48, 0)  # Width is set to 48, height is adjusted proportionally
    img_thumb.save(filename='thumb.jpg')

