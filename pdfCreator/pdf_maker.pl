#!/usr/bin/perl

use strict;
use warnings;
use PDF::API2;
use PDF::Table;
use Lingua::EN::Numbers;
#use Lingua::EN::Numbers qw(num2en);

my $pdf = PDF::API2->new();

my $page = $pdf->page();
my ($words,$number) = '';
$number = 890;
# Set the page dimensions (width, height)
$page->mediabox('A4');

# Add a font
my $font = $pdf->corefont('Helvetica-Bold');

# Add content to the page
my $text = $page->text();
$text->font($font, 18);
$text->translate(250, 750);
$text->text('VAT Invoice');

$text->font($font, 15);
$text->translate(220, 730);
$text->text('Mauritius Telecome Ltd');

$text->font($font, 18);
$text->translate(150, 710);
$text->text('VAT :123456');

$text->font($font, 18);
$text->translate(350, 710);
$text->text('/BRN :765432');

$page->gfx->move(25, 705);
$page->gfx->line(575, 705);
$page->gfx->stroke;


$text->font($font, 14);
$text->translate(100, 680);
$text->text('Customer Name :');

$text->font($font, 14);
$text->translate(350, 680);
$text->text('vimal');

$text->font($font, 14);
$text->translate(100, 660);
$text->text('Customer Code');

$text->font($font, 14);
$text->translate(350, 660);
$text->text('C1234567');

$text->font($font, 14);
$text->translate(100, 640);
$text->text('Address');

$text->font($font, 14);
$text->translate(350, 640);
$text->text('------------------');

$page->gfx->move(25, 635);
$page->gfx->line(575, 635);
$page->gfx->stroke;


$text->font($font, 14);
$text->translate(100, 605);
$text->text('Street :');

$text->font($font, 14);
$text->translate(350, 605);
$text->text('bangok');

$text->font($font, 14);
$text->translate(100, 585);
$text->text('Sub Locality');

$text->font($font, 14);
$text->translate(350, 585);
$text->text('asfdsdfae');

$text->font($font, 14);
$text->translate(100, 565);
$text->text('Locality');

$text->font($font, 14);
$text->translate(350, 565);
$text->text('ksdnkn');

$page->gfx->move(25, 560);
$page->gfx->line(575, 560);
$page->gfx->stroke;

$text->font($font, 14);
$text->translate(100, 530);
$text->text('Purchase Number');

$text->font($font, 14);
$text->translate(350, 530);
$text->text('------------------');

$text->font($font, 14);
$text->translate(100, 510);
$text->text('Recipt Number');

$text->font($font, 14);
$text->translate(350, 510);
$text->text('------------------');

$text->font($font, 14);
$text->translate(100, 490);
$text->text('Transaction Number');

$text->font($font, 14);
$text->translate(350, 490);
$text->text('------------------');

$text->font($font, 14);
$text->translate(100, 470);
$text->text('Transaction Date');

$text->font($font, 14);
$text->translate(350, 470);
$text->text('------------------');

$text->font($font, 14);
$text->translate(100, 450);
$text->text('Custome VAT No');

$text->font($font, 14);
$text->translate(350, 450);
$text->text('------------------');

$text->font($font, 14);
$text->translate(100, 430);
$text->text('Customer BRN');

$text->font($font, 14);
$text->translate(350, 430);
$text->text('------------------');

$text->font($font, 14);
$text->translate(100, 410);
$text->text('Location');

$text->font($font, 14);
$text->translate(350, 410);
$text->text('------------------');

$text->font($font, 14);
$text->translate(100, 390);
$text->text('User Name');

$text->font($font, 14);
$text->translate(350, 390);
$text->text('------------------');

$text->font($font, 14);
$text->translate(100, 370);
$text->text('Mobile Number');

$text->font($font, 14);
$text->translate(350, 370);
$text->text('------------------');

$text->font($font, 14);
$text->translate(100, 280);
$text->text('Remarks : ');

#$words = num2en($number);
$words = Lingua::EN::Numbers::num2en($number);

$text->font($font, 14);
$text->translate(350, 280);
$text->text('------------------');

$text->font($font, 14);
$text->translate(100, 260);
$text->text('AMT in Words : ');

$text->font($font, 14);
$text->translate(210, 260);
$text->text("$words only");

$page->gfx->move(25, 230);
$page->gfx->line(575, 230);
$page->gfx->stroke;

#$text->font($font, 12);
#$text->translate(100, 350);
#$text->text('Items:');

=cutmy @items = (
    'E-Voucher: 100',
    'Discount: 20',
    'total: 80',
);

my $y = 590;
foreach my $item (@items) {
    $text->font($font, 12);
    $text->translate(120, $y);
    $text->text($item);
    $y -= 20;
}
=cut
my @table_data = (
    ['Payment Method', 'Recharge AMT','Status','Balance'],
    ['Cash', $number,'Success',"$number units"],
);
=cut
my @another_table_data = (
    ['Product', 'Stock'],
    ['Product A', '100'],
    ['Product B', '50'],
    ['Product C', '75'],
);
my @table_data1 = (
    ['Description', 'Quantity','Price','Amount'],
    ['QWERTY', '123','$199','$189'],
);
=cut
my $table_x = 100;
my $table_y = 325;
my $cell_width = 100;
my $cell_height = 20;
my $table_border = 1;

my $another_table_x = 450;



sub create_table {
    my ($data, $x, $y, $col_width) = @_;
    
    for my $col (0 .. $#{$data->[0]}) {
        $page->gfx->rect($x + $col * $cell_width, $y, $cell_width, $cell_height);
        $page->gfx->stroke;
        $text->font($font, 12);
        $text->translate($x + $col * $cell_width + 5, $y + 5);
        $text->text($data->[0][$col]);
    }

    for my $row (1 .. $#{$data}) {
        for my $col (0 .. $#{$data->[$row]}) {
            $page->gfx->rect($x + $col * $cell_width, $y - $row * $cell_height, $cell_width, $cell_height);
            $page->gfx->stroke;
            $text->font($font, 12);
            $text->translate($x + $col * $cell_width + 5, $y - $row * $cell_height + 5);
            $text->text($data->[$row][$col]);
        }
    }
}

######################################################################################################

=cut
for my $col (0 .. $#{$table_data[0]}) {
    $page->gfx->rect($table_x + $col * $cell_width, $table_y, $cell_width, $cell_height);
    $page->gfx->stroke;
    $text->font($font, 12);
    $text->translate($table_x + $col * $cell_width + 5, $table_y + 5);
    $text->text($table_data[0][$col]);
}

for my $row (1 .. $#table_data) {
    for my $col (0 .. $#{$table_data[$row]}) {
        $page->gfx->rect($table_x + $col * $cell_width, $table_y - $row * $cell_height, $cell_width, $cell_height);
        $page->gfx->stroke;
        $text->font($font, 12);
        $text->translate($table_x + $col * $cell_width + 5, $table_y - $row * $cell_height + 5);
        $text->text($table_data[$row][$col]);
    }
}
=cut
##########################################################################################################

create_table(\@table_data, $table_x, $table_y);
#create_table(\@another_table_data, 300, $table_y - ($cell_height * (scalar @table_data) + 5));
#create_table(\@table_data1, $table_x, $table_y - ($cell_height * (scalar @another_table_data) + 50));

# Save the PDF to a file
$pdf->saveas('receipt.pdf');

print "Receipt PDF created.\n";

